@ControllerAdvice
@Slf4jpublic
class AppExceptionHandler {
    @ExceptionHandler(DomainException.class)
    public ResponseEntity<Error> handleDomainException(DomainException exception) {
        log.error("Domain exception occurred", exception);
        HttpStatus httpStatus = HttpStatus.valueOf(exception.getCode().getStatus());
        Error body = new Error(httpStatus.value(), exception.getMessage());
        return new ResponseEntity<>(body, httpStatus);
    }