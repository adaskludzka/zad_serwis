package pl.teb.serwis3d.infrastructure.converter;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;
import pl.teb.serwis3d.api.dto.response.UserResponse;
import pl.teb.serwis3d.infrastructure.entity.User;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.ERROR
)
public interface DtoMapper {

    UserResponse map(User user);
}
