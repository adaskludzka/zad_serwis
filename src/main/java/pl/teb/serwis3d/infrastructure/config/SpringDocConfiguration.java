package pl.teb.serwis3d.infrastructure.config;

import io.swagger.v3.core.converter.ModelConverters;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.responses.ApiResponse;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.teb.serwis3d.api.exeption.AppExceptionHandler;


@Configuration
public class SpringDocConfiguration {
    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder().group("serwis3d-public").pathsToMatch("/user/**").build();
    }

    @Bean
    public OpenAPI applicationOpenAPI() {
        return new OpenAPI().info(new Info().title("Serwis3d API").description("Serwis3d API application").version("v0.0.1"));
    }

    @Bean
    public OpenApiCustomiser openapiCustomizer() {
        var errSchema = ModelConverters.getInstance().readAllAsResolvedSchema(AppExceptionHandler.Error.class).schema;
        var cont = new Content().addMediaType(org.springframework.http.MediaType.APPLICATION_JSON_VALUE, new MediaType().schema(errSchema));
        return api -> api.getPaths().forEach((key, value) -> value.readOperations().forEach(operation -> {
            operation.getResponses().addApiResponse("400", new ApiResponse().description("Bad request").content(cont));
            operation.getResponses().addApiResponse("404", new ApiResponse().description("Not found").content(cont));
            operation.getResponses().addApiResponse("500", new ApiResponse().description("Internal error").content(cont));
        }));
    }
}
