package pl.teb.serwis3d.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.teb.serwis3d.infrastructure.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
