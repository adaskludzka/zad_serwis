package pl.teb.serwis3d;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Serwis3dApplication {

    public static void main(String[] args) {
        SpringApplication.run(Serwis3dApplication.class, args);
    }

}
