package pl.teb.serwis3d.api.dto.response;

import jakarta.validation.constraints.Email;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Data
public class UserResponse {

    private String uuid;
    private String firstname;
    private String lastname;
    @Email(message = "Email is not valid.", regexp = "\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b")
    private String email;
    private String password;

}
