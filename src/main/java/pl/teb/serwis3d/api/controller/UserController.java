package pl.teb.serwis3d.api.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.teb.serwis3d.api.dto.request.UserRequest;
import pl.teb.serwis3d.api.dto.response.UserResponse;
import pl.teb.serwis3d.domain.service.UserService;
import pl.teb.serwis3d.infrastructure.converter.DtoMapper;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;
    private final DtoMapper mapper;

    @PostMapping
    public ResponseEntity<UserResponse> createUser(@RequestBody UserRequest userRequest){
        return ResponseEntity.ok(mapper.map(userService.createUser(
                userRequest.getFirstname(), userRequest.getLastname(), userRequest.getEmail(),
                userRequest.getPassword())));
    }
}
